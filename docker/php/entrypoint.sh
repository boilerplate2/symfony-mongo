#!/bin/sh

find . -type d -exec chmod 755 {} \;
find . -type f -exec chmod 644 {} \;
find bin/ -type f -exec chmod 644 {} \;
chmod u+x docker-compose.yml
chmod -R 777 var/

composer install

bin/console doctrine:mongodb:schema:update

php-fpm
